import React, { useState, useEffect } from 'react';

function HatsForm({ getHats }) {
    const [style_name, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [fabric, setFabric] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);

    useEffect(() => {
        async function getLocations() {
            const url = 'http://localhost:8100/api/locations/';

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setLocations(data.locations);
            }
        }
        getLocations();
    }, []);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            style_name,
            color,
            fabric,
            picture_url,
            location,
        };

        const locationUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            getHats();

            setStyleName('');
            setColor('');
            setFabric('');
            setPictureUrl('');
            setLocation('');
        }
    }

    function handleChangeStyleName(event) {
        const value = event.target.value;
        setStyleName(value);
    }

    function handleChangeColor(event) {
        const value = event.target.value;
        setColor(value);
    }

    function handleChangeFabric(event) {
        const value = event.target.value;
        setFabric(value);
    }

    function handleChangePictureUrl(event) {
        const value = event.target.value;
        setPictureUrl(value);
    }

    function handleChangeLocation(event) {
        const value = event.target.value;
        setLocation(value);
    }

    return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id='create-hat-form'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleChangeStyleName} value={style_name} placeholder='Style name' required type='char' name='style_name' id='style_name' className='form-control'/>
                            <label htmlFor='style_name'>Style name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleChangeColor} value={color} placeholder="Color" required type="char" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleChangeFabric} value={fabric} placeholder="Fabric" required type="char" name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleChangePictureUrl} value={picture_url} placeholder="Pictur url" required type="url" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleChangeLocation} value={location} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>{location.closet_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HatsForm;
