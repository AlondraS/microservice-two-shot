import React, { useState, useEffect } from 'react';

function ShoeForm() {
    const [model, setModel] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);

    useEffect(() => {
        async function getBins() {
            const url = 'http://localhost:8100/api/bins/';

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setBins(data.bins);
            }
        }
        getBins();
    }, []);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            model,
            color,
            manufacturer,
            picture_url,
            bin,
        };

        const locationUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if(response.ok) {
            setModel('');
            setManufacturer('');
            setBin('');
            setPictureUrl('');
            setColor('');
        }
    }

    const handleChangeBin = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleChangeModel = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const handleChangePictureUrl = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new shoe</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">

                <div className="form-floating mb-3">
                    <input onChange={handleChangeModel} value={model} placeholder="Model" required type="text" name="model" id="model" className="form-control" />
                    <label htmlFor="model">Model</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleChangeManufacturer} value ={manufacturer}placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleChangeColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleChangePictureUrl} value={picture_url} placeholder="Picture URL" required type="text" name="pictureurl" id="pictureurl" className="form-control" />
                    <label htmlFor="pictureurl">Picture URL</label>
                </div>

                <div className="mb-3">
                    <select onChange={handleChangeBin} value={bin} required name="bin" id="bin" className="form-select">
                    <option value="">Choose a bin</option>
                    {bins.map((bin) => {
                        return (
                        <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                        )
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        )
    }

export default ShoeForm;
