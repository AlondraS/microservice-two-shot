import React, { useState, useEffect } from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsForm from './HatsForm';
import HatsList from './HatsList';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';

function App() {
  const [hats, setHats] = useState([]);
  const [shoeList, setShoeList ] = useState([]);

  async function getHats() {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  }
  async function getShoeList() {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setShoeList(data.shoes);
    }
  }

  useEffect(() => {
    getHats();
    getShoeList();
  }, []);
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatsForm getHats={getHats} />}/>
          <Route path="/hats" element={<HatsList hats={hats} getHats={getHats} />}/>
          <Route path="/shoes/" element={<ShoeList shoes={shoeList} />} />
          <Route path="/shoes/new/" element={<ShoeForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
