function ShoeList(props) {
    const deleteShoe = async (shoe) => {
        const url = `http://localhost:8080/api/shoes/${shoe.id}/`;
        const fetchConfig = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                window.location.reload();
            }
        } catch (error) {
            console.log('Failed to delete shoe');
        }
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                    <th>Closet</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes && props.shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>{ shoe.model }</td>
                            <td>{ shoe.manufacturer }</td>
                            <td>{ shoe.color }</td>
                            <td>{ shoe.picture_url }</td>
                            <td>{ shoe.bin.name }</td>
                            <td>
                                <button onClick={() => deleteShoe(shoe)}>
                                    Delete
                                </button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    );
}

export default ShoeList