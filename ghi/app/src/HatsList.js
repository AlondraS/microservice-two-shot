function HatList(props) {
    const deleteHat = async (hat) => {
        const url = `http://localhost:8090/api/hats/${hat.id}/`;
        const fetchConfig = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                window.location.reload();
                HatList();
            }
        } catch (error) {
            console.log('Failed to delete hat');
        }
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Fabric</th>
                    <th>Picture</th>
                    <th>Closet</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map( hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.picture_url }</td>
                            <td>{ hat.location.closet_name }</td>
                            <td>
                                <button onClick={() => deleteHat(hat)}>
                                    Delete
                                </button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    );
}

export default HatList
