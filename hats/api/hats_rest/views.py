from common.json import ModelEncoder
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["name", "vo_id"]


class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "color",
        "fabric",
        "picture_url",
        "id",
        "location"
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            location_object = LocationVO.objects.get(vo_id=content["location"])
            content['location'] = location_object

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location vo_id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False,
        )
    else:
        if request.method == "GET":
            hat = Hat.objects.all()
            return JsonResponse(
                {"hats": hat},
                encoder=HatsListEncoder,
            )


@require_http_methods(["DELETE", "GET"])
def api_show_hats(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatsListEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatsListEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Hat does not exist"})
