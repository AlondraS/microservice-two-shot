from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    vo_id = models.PositiveSmallIntegerField()
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = "LocationVO"
        verbose_name_plural = "LocationVOs"


class Hat(models.Model):
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    fabric = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.PROTECT,
        null=True
    )

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.style_name} - {self.color}/{self.fabric}"

    class Meta:
        ordering = ("style_name", "color", "fabric")
        verbose_name = "Hat"
        verbose_name_plural = "Hats"
